//
// m3d.h
//
// fork of linmath.h
// changes
//  - made all function names lower case
//  - shortened function prefixes
//  - renamed orthonormalize to orthonorm
//  - renamed *_mul_inner to *_dot
//  - renamed *_mul_cross to *_cross
//  - renamed *_dup to *_set
//  - added scalar add, sub
//  - added len2 functions
//  - added fuzzy comparisons
//  - added run-time "constructors"
//  - added m4_scaling
//  - added loads of const
//  - added radian/degree functions
//  - added wrap/lerp/nlerp/slerp functions
//  - added project/unproject functions
//  - added frustum functions
//  - added min/max term functions
//  - added 1d setters
//  - added vector cmp functions
//  - removed min/max vector functions

#ifndef M3D_H
#define M3D_H

#include <math.h>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062
#endif

#ifndef M3D_EPSILON
#define M3D_EPSILON 1e-4
#endif

#ifndef __cplusplus
#define bool int
#endif

static inline float rad(float a) {
	return a*M_PI/180.0f;
}

static inline float deg(float a) {
	return a*180.0f/M_PI;
}

// wrap t within the range [0.0f, m), similar to integer mod
static inline float wrap(float t, float m) {
	return t - m*floorf(t/m);
}

static inline float lerp(float a, float t, float b) {
	return a + (b - a)*t;
}

inline float f_eq(float a, float b) {
	return fabs(a - b) < M3D_EPSILON;
}

static inline int f_cmp(float a, float b) {
	if(f_eq(a, b)) return 0;
	if(a > b) return 1;
	return -1;
}

typedef float frustum[6][4];
typedef float f2[2];
typedef float f3[3];
typedef float f4[4];
typedef float f44[4][4];

typedef f2 vec2;
typedef f3 vec3;
typedef f4 vec4;
typedef f44 mat4;
typedef f4 quat;

// useful for temp variables in function calls
#define V2(x, y)       (const f2) { (x), (y) }
#define V3(x, y, z)    (const f3) { (x), (y), (z) }
#define V4(x, y, z, w) (const f3) { (x), (y), (z), (w) }
#define M4(m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33) (const f44) { { (m00), (m01), (m02), (m03) }, { (m10), (m11), (m12), (m13) }, { (m20), (m21), (m22), (m23) }, { (m30), (m31), (m32), (m33) } }
#define Q4(x, y, z, w) V4(x, y, z, w)

// setters
static inline void v2(f2 r, float x, float y) {
	r[0] = x; r[1] = y;
}
static inline void v3(f3 r, float x, float y, float z) {
	r[0] = x; r[1] = y; r[2] = z;
}
static inline void v4(f4 r, float x, float y, float z, float w) {
	r[0] = x; r[1] = y; r[2] = z; r[3] = w;
}
static inline void v2(f2 r, float f) {
	r[0] = r[1] = f;
}
static inline void v3(f3 r, float f) {
	r[0] = r[1] = r[2] = f;
}
static inline void v4(f4 r, float f) {
	r[0] = r[1] = r[2] = r[3] = f;
}
static inline void m4(f44 r,
	float m00, float m01, float m02, float m03,
	float m10, float m11, float m12, float m13,
	float m20, float m21, float m22, float m23,
	float m30, float m31, float m32, float m33) {
	r[0][0] = m00; r[0][1] = m01; r[0][2] = m02; r[0][3] = m03;
	r[1][0] = m10; r[1][1] = m11; r[1][2] = m12; r[1][3] = m13;
	r[2][0] = m20; r[2][1] = m21; r[2][2] = m22; r[2][3] = m23;
	r[3][0] = m30; r[3][1] = m31; r[3][2] = m32; r[3][3] = m33;
}
#define q(r, x, y, z, w) v4((r), (x), (y), (z), (w))

// vectors
#define LINMATH_H_DEFINE_VEC(n) \
static inline bool v##n##_eq(const f##n a, const f##n b) { \
	int i; \
	for(i=0; i<n; ++i) \
		if(!f_eq(a[i], b[i])) return 0; \
	return 1; \
} \
static inline int v##n##_cmp(const f##n a, const f##n b) { \
	int i; \
	for(i=0; i<n; ++i) { \
		int cmp = f_cmp(a[i], b[i]); \
		if(cmp) return cmp; \
	} \
	return 0; \
} \
static inline void v##n##_set(f##n r, const f##n a) { \
	int i; \
	for(i=0; i<n; ++i) \
		r[i] = a[i]; \
} \
static inline void v##n##_add(f##n r, const f##n a, const f##n b) { \
	int i; \
	for(i=0; i<n; ++i) \
		r[i] = a[i] + b[i]; \
} \
static inline void v##n##_adds(f##n r, const f##n a, const float s) { \
	int i; \
	for(i=0; i<n; ++i) \
		r[i] = a[i] + s; \
} \
static inline void v##n##_sub(f##n r, const f##n a, const f##n b) { \
	int i; \
	for(i=0; i<n; ++i) \
		r[i] = a[i] - b[i]; \
} \
static inline void v##n##_subs(f##n r, const f##n a, const float s) { \
	int i; \
	for(i=0; i<n; ++i) \
		r[i] = a[i] - s; \
} \
static inline void v##n##_scale(f##n r, const f##n v, const float s) { \
	int i; \
	for(i=0; i<n; ++i) \
		r[i] = v[i] * s; \
} \
static inline float v##n##_dot(const f##n a, const f##n b) { \
	float p = 0.; \
	int i; \
	for(i=0; i<n; ++i) \
		p += b[i]*a[i]; \
	return p; \
} \
static inline float v##n##_len2(const f##n v) { \
	return v##n##_dot(v,v); \
} \
static inline float v##n##_len(const f##n v) { \
	return sqrtf(v##n##_len2(v)); \
} \
static inline void v##n##_norm(f##n r, const f##n v) { \
	float k = 1.0f / v##n##_len(v); \
	v##n##_scale(r, v, k); \
} \
static inline void v##n##_lerp(f##n r, const f##n a, float t, const f##n b) { \
	int i; \
	for(i=0; i<n; ++i) \
		r[i] = lerp(a[i], t, b[i]); \
} \
static inline void v##n##_nlerp(f##n r, const f##n a, float t, const f##n b) { \
	v##n##_lerp(r, a, t, b); \
	v##n##_norm(r, r); \
} \
static inline float v##n##_max_term(const f##n a) { \
	float m = a[0]; \
	int i; \
	for(i=1; i<n; ++i) \
		m = (m < a[i] ? a[i] : m); \
	return m; \
} \
static inline float v##n##_min_term(const f##n a) { \
	float m = a[0]; \
	int i; \
	for(i=1; i<n; ++i) \
		m = (m > a[i] ? a[i] : m); \
	return m; \
}

LINMATH_H_DEFINE_VEC(2)
LINMATH_H_DEFINE_VEC(3)
LINMATH_H_DEFINE_VEC(4)

// specialized vectors
static inline void v3_cross(f3 r, const f3 a, const f3 b) {
	r[0] = a[1]*b[2] - a[2]*b[1];
	r[1] = a[2]*b[0] - a[0]*b[2];
	r[2] = a[0]*b[1] - a[1]*b[0];
}

static inline void v3_reflect(f3 r, const f3 v, const f3 n) {
	float p = 2.f*v3_dot(v, n);
	int i;
	for(i=0;i<3;++i)
		r[i] = v[i] - p*n[i];
}

static inline void v4_cross(f4 r, const f4 a, const f4 b) {
	r[0] = a[1]*b[2] - a[2]*b[1];
	r[1] = a[2]*b[0] - a[0]*b[2];
	r[2] = a[0]*b[1] - a[1]*b[0];
	r[3] = 1.f;
}

static inline void v4_reflect(f4 r, const f4 v, const f4 n) {
	float p  = 2.f*v4_dot(v, n);
	int i;
	for(i=0;i<4;++i)
		r[i] = v[i] - p*n[i];
}

// matricies
static inline bool m4_eq(const f44 M, const f44 N) {
	int i, j;
	for(i=0; i<4; ++i)
		for(j=0; j<4; ++j)
			if(!f_eq(M[i][j], N[i][j])) return 0;
	return 1;
}

static inline void m4_identity(f44 M) {
	int i, j;
	for(i=0; i<4; ++i)
		for(j=0; j<4; ++j)
			M[i][j] = i==j ? 1.f : 0.f;
}

static inline void m4_set(f44 M, const f44 N) {
	int i, j;
	for(i=0; i<4; ++i)
		for(j=0; j<4; ++j)
			M[i][j] = N[i][j];
}

static inline void m4_row(f4 r, const f44 M, int i) {
	int k;
	for(k=0; k<4; ++k)
		r[k] = M[k][i];
}

static inline void m4_col(f4 r, const f44 M, int i) {
	int k;
	for(k=0; k<4; ++k)
		r[k] = M[i][k];
}

static inline void m4_transpose(f44 M, const f44 N) {
	int i, j;
	for(j=0; j<4; ++j)
		for(i=0; i<4; ++i)
			M[i][j] = N[j][i];
}

static inline void m4_add(f44 M, const f44 a, const f44 b) {
	int i;
	for(i=0; i<4; ++i)
		v4_add(M[i], a[i], b[i]);
}

static inline void m4_sub(f44 M, const f44 a, const f44 b) {
	int i;
	for(i=0; i<4; ++i)
		v4_sub(M[i], a[i], b[i]);
}

static inline void m4_scaling(f44 M, float x, float y, float z) {
	m4(M,
		   x, 0.0f, 0.0f, 0.0f,
		0.0f,    y, 0.0f, 0.0f,
		0.0f, 0.0f,    z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

static inline void m4_scale(f44 M, const f44 a, float k) {
	int i;
	for(i=0; i<4; ++i)
		v4_scale(M[i], a[i], k);
}

static inline void m4_scale_aniso(f44 M, const f44 a, float x, float y, float z) {
	int i;
	v4_scale(M[0], a[0], x);
	v4_scale(M[1], a[1], y);
	v4_scale(M[2], a[2], z);
	for(i = 0; i < 4; ++i) {
		M[3][i] = a[3][i];
	}
}

static inline void m4_mul(f44 M, const f44 a, const f44 b) {
	f44 temp;
	int k, r, c;
	for(c=0; c<4; ++c) for(r=0; r<4; ++r) {
		temp[c][r] = 0.f;
		for(k=0; k<4; ++k)
			temp[c][r] += a[k][r] * b[c][k];
	}
	m4_set(M, temp);
}

static inline void m4_mul_v4(f4 r, const f44 M, const f4 v) {
	int i, j;
	for(j=0; j<4; ++j) {
		r[j] = 0.f;
		for(i=0; i<4; ++i)
			r[j] += M[i][j] * v[i];
	}
}

static inline void m4_translate(f44 T, float x, float y, float z) {
	m4(T,
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		   x,    y,    z, 1.0f
	);
}

static inline void m4_translate_in_place(f44 M, float x, float y, float z) {
	f4 t = {x, y, z, 0};
	f4 r;
	int i;
	for (i = 0; i < 4; ++i) {
		m4_row(r, M, i);
		M[3][i] += v4_dot(r, t);
	}
}

static inline void m4_from_v3_mul_outer(f44 M, const f3 a, const f3 b) {
	int i, j;
	for(i=0; i<4; ++i) for(j=0; j<4; ++j)
		M[i][j] = i<3 && j<3 ? a[i] * b[j] : 0.f;
}

static inline void m4_rotate(f44 R, const f44 M, float x, float y, float z, float angle) {
	float s = sinf(angle);
	float c = cosf(angle);
	f3 u = {x, y, z};

	if(v3_len(u) > M3D_EPSILON) {
		v3_norm(u, u);
		f44 T;
		m4_from_v3_mul_outer(T, u, u);

		f44 S = {
			{     0,  u[2], -u[1], 0 },
			{ -u[2],     0,  u[0], 0 },
			{  u[1], -u[0],     0, 0 },
			{     0,     0,     0, 0 }
		};
		m4_scale(S, S, s);

		f44 C;
		m4_identity(C);
		m4_sub(C, C, T);

		m4_scale(C, C, c);

		m4_add(T, T, C);
		m4_add(T, T, S);

		T[3][3] = 1.;
		m4_mul(R, M, T);
	} else {
		m4_set(R, M);
	}
}

static inline void m4_rotate_x(f44 Q, const f44 M, float angle) {
	float s = sinf(angle);
	float c = cosf(angle);
	f44 R = {
		{1.f, 0.f, 0.f, 0.f},
		{0.f,   c,   s, 0.f},
		{0.f,  -s,   c, 0.f},
		{0.f, 0.f, 0.f, 1.f}
	};
	m4_mul(Q, M, R);
}

static inline void m4_rotate_y(f44 Q, const f44 M, float angle) {
	float s = sinf(angle);
	float c = cosf(angle);
	f44 R = {
		{   c, 0.f,   s, 0.f},
		{ 0.f, 1.f, 0.f, 0.f},
		{  -s, 0.f,   c, 0.f},
		{ 0.f, 0.f, 0.f, 1.f}
	};
	m4_mul(Q, M, R);
}

static inline void m4_rotate_z(f44 Q, const f44 M, float angle) {
	float s = sinf(angle);
	float c = cosf(angle);
	f44 R = {
		{   c,   s, 0.f, 0.f},
		{  -s,   c, 0.f, 0.f},
		{ 0.f, 0.f, 1.f, 0.f},
		{ 0.f, 0.f, 0.f, 1.f}
	};
	m4_mul(Q, M, R);
}

static inline void m4_invert(f44 T, const f44 M) {
	float s[6];
	float c[6];
	s[0] = M[0][0]*M[1][1] - M[1][0]*M[0][1];
	s[1] = M[0][0]*M[1][2] - M[1][0]*M[0][2];
	s[2] = M[0][0]*M[1][3] - M[1][0]*M[0][3];
	s[3] = M[0][1]*M[1][2] - M[1][1]*M[0][2];
	s[4] = M[0][1]*M[1][3] - M[1][1]*M[0][3];
	s[5] = M[0][2]*M[1][3] - M[1][2]*M[0][3];

	c[0] = M[2][0]*M[3][1] - M[3][0]*M[2][1];
	c[1] = M[2][0]*M[3][2] - M[3][0]*M[2][2];
	c[2] = M[2][0]*M[3][3] - M[3][0]*M[2][3];
	c[3] = M[2][1]*M[3][2] - M[3][1]*M[2][2];
	c[4] = M[2][1]*M[3][3] - M[3][1]*M[2][3];
	c[5] = M[2][2]*M[3][3] - M[3][2]*M[2][3];

	// assumes it is invertible
	float idet = 1.0f/( s[0]*c[5]-s[1]*c[4]+s[2]*c[3]+s[3]*c[2]-s[4]*c[1]+s[5]*c[0] );

	T[0][0] = ( M[1][1] * c[5] - M[1][2] * c[4] + M[1][3] * c[3]) * idet;
	T[0][1] = (-M[0][1] * c[5] + M[0][2] * c[4] - M[0][3] * c[3]) * idet;
	T[0][2] = ( M[3][1] * s[5] - M[3][2] * s[4] + M[3][3] * s[3]) * idet;
	T[0][3] = (-M[2][1] * s[5] + M[2][2] * s[4] - M[2][3] * s[3]) * idet;

	T[1][0] = (-M[1][0] * c[5] + M[1][2] * c[2] - M[1][3] * c[1]) * idet;
	T[1][1] = ( M[0][0] * c[5] - M[0][2] * c[2] + M[0][3] * c[1]) * idet;
	T[1][2] = (-M[3][0] * s[5] + M[3][2] * s[2] - M[3][3] * s[1]) * idet;
	T[1][3] = ( M[2][0] * s[5] - M[2][2] * s[2] + M[2][3] * s[1]) * idet;

	T[2][0] = ( M[1][0] * c[4] - M[1][1] * c[2] + M[1][3] * c[0]) * idet;
	T[2][1] = (-M[0][0] * c[4] + M[0][1] * c[2] - M[0][3] * c[0]) * idet;
	T[2][2] = ( M[3][0] * s[4] - M[3][1] * s[2] + M[3][3] * s[0]) * idet;
	T[2][3] = (-M[2][0] * s[4] + M[2][1] * s[2] - M[2][3] * s[0]) * idet;

	T[3][0] = (-M[1][0] * c[3] + M[1][1] * c[1] - M[1][2] * c[0]) * idet;
	T[3][1] = ( M[0][0] * c[3] - M[0][1] * c[1] + M[0][2] * c[0]) * idet;
	T[3][2] = (-M[3][0] * s[3] + M[3][1] * s[1] - M[3][2] * s[0]) * idet;
	T[3][3] = ( M[2][0] * s[3] - M[2][1] * s[1] + M[2][2] * s[0]) * idet;
}

static inline void m4_orthonorm(f44 R, const f44 M) {
	m4_set(R, M);
	float s = 1.;
	f3 h;

	v3_norm(R[2], R[2]);

	s = v3_dot(R[1], R[2]);
	v3_scale(h, R[2], s);
	v3_sub(R[1], R[1], h);
	v3_norm(R[2], R[2]);

	s = v3_dot(R[1], R[2]);
	v3_scale(h, R[2], s);
	v3_sub(R[1], R[1], h);
	v3_norm(R[1], R[1]);

	s = v3_dot(R[0], R[1]);
	v3_scale(h, R[1], s);
	v3_sub(R[0], R[0], h);
	v3_norm(R[0], R[0]);
}

static inline void m4_frustum(f44 M, float l, float r, float b, float t, float n, float f) {
	M[0][0] = 2.f*n/(r-l);
	M[0][1] = M[0][2] = M[0][3] = 0.f;

	M[1][1] = 2.*n/(t-b);
	M[1][0] = M[1][2] = M[1][3] = 0.f;

	M[2][0] = (r+l)/(r-l);
	M[2][1] = (t+b)/(t-b);
	M[2][2] = -(f+n)/(f-n);
	M[2][3] = -1.f;

	M[3][2] = -2.f*(f*n)/(f-n);
	M[3][0] = M[3][1] = M[3][3] = 0.f;
}

static inline void m4_ortho(f44 M, float l, float r, float b, float t, float n, float f) {
	M[0][0] = 2.f/(r-l);
	M[0][1] = M[0][2] = M[0][3] = 0.f;

	M[1][1] = 2.f/(t-b);
	M[1][0] = M[1][2] = M[1][3] = 0.f;

	M[2][2] = -2.f/(f-n);
	M[2][0] = M[2][1] = M[2][3] = 0.f;

	M[3][0] = -(r+l)/(r-l);
	M[3][1] = -(t+b)/(t-b);
	M[3][2] = -(f+n)/(f-n);
	M[3][3] = 1.f;
}

static inline void m4_perspective(f44 m, float y_fov, float aspect, float n, float f) {
	/* NOTE: Degrees are an unhandy unit to work with.
	 * linmath.h uses radians for everything! */
	float const a = 1.f / tanf(y_fov / 2.f);

	m[0][0] = a / aspect;
	m[0][1] = 0.f;
	m[0][2] = 0.f;
	m[0][3] = 0.f;

	m[1][0] = 0.f;
	m[1][1] = a;
	m[1][2] = 0.f;
	m[1][3] = 0.f;

	m[2][0] = 0.f;
	m[2][1] = 0.f;
	m[2][2] = -((f + n) / (f - n));
	m[2][3] = -1.f;

	m[3][0] = 0.f;
	m[3][1] = 0.f;
	m[3][2] = -((2.f * f * n) / (f - n));
	m[3][3] = 0.f;
}

static inline void m4_look_at(f44 m, const f3 eye, const f3 center, const f3 up) {
	/* Adapted from Android's OpenGL Matrix.java.                        */
	/* See the OpenGL GLUT documentation for gluLookAt for a description */
	/* of the algorithm. We implement it in a straightforward way:       */

	/* TODO: The negation of of can be spared by swapping the order of
	 *       operands in the following cross products in the right way. */
	f3 f;
	v3_sub(f, center, eye);
	v3_norm(f, f);

	f3 s;
	v3_cross(s, f, up);
	v3_norm(s, s);

	f3 t;
	v3_cross(t, s, f);

	m[0][0] =  s[0];
	m[0][1] =  t[0];
	m[0][2] = -f[0];
	m[0][3] =   0.f;

	m[1][0] =  s[1];
	m[1][1] =  t[1];
	m[1][2] = -f[1];
	m[1][3] =   0.f;

	m[2][0] =  s[2];
	m[2][1] =  t[2];
	m[2][2] = -f[2];
	m[2][3] =   0.f;

	m[3][0] =  0.f;
	m[3][1] =  0.f;
	m[3][2] =  0.f;
	m[3][3] =  1.f;

	m4_translate_in_place(m, -eye[0], -eye[1], -eye[2]);
}

// quaternions
#define q_eq(a, b)          v4_eq((a), (b))
#define q_set(a, b)         v4_set((a), (b))
#define q_add(r, a, b)      v4_add((r), (a), (b))
#define q_adds(r, a, b)     v4_adds((r), (a), (b))
#define q_sub(r, a, b)      v4_sub((r), (a), (b))
#define q_subs(r, a, b)     v4_subs((r), (a), (b))
#define q_scale(r, v, s)    v4_scale((r), (v), (s))
#define q_dot(a, b)         v4_dot((a), (b))
#define q_norm(r, a)        v4_norm((r), (a))
#define q_lerp(r, a, t, b)  v4_lerp((r), (a), (t), (b))
#define q_nlerp(r, a, t, b) v4_nlerp((r), (a), (t), (b))

// adapted from http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/slerp/
// note: http://number-none.com/product/Understanding%20Slerp,%20Then%20Not%20Using%20It/
static inline void q_slerp(f4 r, const f4 a, float t, const f4 b) {
	// calculate angle between them.
	float cos_half_theta = q_dot(a, b);
	// if a = b or a = -b then theta = 0 and we can return a
	if(fabs(cos_half_theta) >= 1.0f) {
		q_set(r, a);
		return;
	}

	// calculate temporary values
	float half_theta = acosf(cos_half_theta);
	float sin_half_theta = sqrtf(1.0f - cos_half_theta*cos_half_theta);
	// if theta = 180 degrees then result is not fully defined
	// we could rotate around any axis normal to a or b
	if(fabs(sin_half_theta) < M3D_EPSILON) {
		r[3] = (a[3]*0.5f + b[3]*0.5f);
		r[0] = (a[0]*0.5f + b[0]*0.5f);
		r[1] = (a[1]*0.5f + b[1]*0.5f);
		r[2] = (a[2]*0.5f + b[2]*0.5f);
		return;
	}

	float ratio_a = sinf((1.0f - t)*half_theta)/sin_half_theta;
	float ratio_b = sinf(t*half_theta)/sin_half_theta;

	// calculate quaternions
	r[3] = (a[3]*ratio_a + b[3]*ratio_b);
	r[0] = (a[0]*ratio_a + b[0]*ratio_b);
	r[1] = (a[1]*ratio_a + b[1]*ratio_b);
	r[2] = (a[2]*ratio_a + b[2]*ratio_b);
}

static inline void q_identity(f4 q) {
	q[0] = q[1] = q[2] = 0.f;
	q[3] = 1.f;
}

static inline void q_mul(f4 r, const f4 p, const f4 q) {
	f3 w;
	v3_cross(r, p, q);
	v3_scale(w, p, q[3]);
	v3_add(r, r, w);
	v3_scale(w, q, p[3]);
	v3_add(r, r, w);
	r[3] = p[3]*q[3] - v3_dot(p, q);
}

static inline void q_conj(f4 r, const f4 q) {
	int i;
	for(i=0; i<3; ++i)
		r[i] = -q[i];
	r[3] = q[3];
}

// if axis is not normalized, the object is incorrectly scaled
static inline void q_rotate(f4 r, float angle, const f3 axis) {
	f3 v;
	v3_scale(v, axis, sinf(angle/2));
	int i;
	for(i=0; i<3; ++i)
		r[i] = v[i];
	r[3] = cosf(angle/2);
}

static inline void q_mul_v3(f3 r, const f4 q, const f3 v) {
/*
 * Method by Fabian 'ryg' Giessen (of Farbrausch)
t = 2 * cross(q.xyz, v)
v' = v + q.w * t + cross(q.xyz, t)
 */
	f3 t;
	f3 q_xyz = {q[0], q[1], q[2]};
	f3 u = {q[0], q[1], q[2]};

	v3_cross(t, q_xyz, v);
	v3_scale(t, t, 2);

	v3_cross(u, q_xyz, t);
	v3_scale(t, t, q[3]);

	v3_add(r, v, t);
	v3_add(r, r, u);
}

static inline void m4_from_q(f44 M, const f4 q) {
	float a = q[3];
	float b = q[0];
	float c = q[1];
	float d = q[2];
	float a2 = a*a;
	float b2 = b*b;
	float c2 = c*c;
	float d2 = d*d;

	M[0][0] = a2 + b2 - c2 - d2;
	M[0][1] = 2.f*(b*c + a*d);
	M[0][2] = 2.f*(b*d - a*c);
	M[0][3] = 0.f;

	M[1][0] = 2*(b*c - a*d);
	M[1][1] = a2 - b2 + c2 - d2;
	M[1][2] = 2.f*(c*d + a*b);
	M[1][3] = 0.f;

	M[2][0] = 2.f*(b*d + a*c);
	M[2][1] = 2.f*(c*d - a*b);
	M[2][2] = a2 - b2 - c2 + d2;
	M[2][3] = 0.f;

	M[3][0] = M[3][1] = M[3][2] = 0.f;
	M[3][3] = 1.f;
}

static inline void m4o_mul_q(f44 R, const f44 M, const f4 q) {
/*  XXX: The way this is written only works for othogonal matrices. */
/* TODO: Take care of non-orthogonal case. */
	q_mul_v3(R[0], q, M[0]);
	q_mul_v3(R[1], q, M[1]);
	q_mul_v3(R[2], q, M[2]);

	R[3][0] = R[3][1] = R[3][2] = 0.f;
	R[3][3] = 1.f;
}

static inline void q_from_m4(f4 q, const f44 M) {
	float r=0.f;
	int i;

	int perm[] = { 0, 1, 2, 0, 1 };
	int *p = perm;

	for(i=0; i<3; i++) {
		float m = M[i][i];
		if(m < r)
			continue;
		m = r;
		p = &perm[i];
	}

	r = sqrtf(1.f + M[p[0]][p[0]] - M[p[1]][p[1]] - M[p[2]][p[2]] );

	if(r < M3D_EPSILON) {
		q[0] = 1.f;
		q[1] = q[2] = q[3] = 0.f;
		return;
	}

	q[0] = r/2.f;
	q[1] = (M[p[0]][p[1]] - M[p[1]][p[0]])/(2.f*r);
	q[2] = (M[p[2]][p[0]] - M[p[0]][p[2]])/(2.f*r);
	q[3] = (M[p[2]][p[1]] - M[p[1]][p[2]])/(2.f*r);
}

// taken from http://www.crownandcutlass.com/features/technicaldetails/frustum.html
static inline void calculate_frustum(frustum frust, f44 cam_projview) {
	float* clip = &cam_projview[0][0];
	float t;

	// extract the numbers for the RIGHT plane
	frust[0][0] = clip[ 3] - clip[ 0];
	frust[0][1] = clip[ 7] - clip[ 4];
	frust[0][2] = clip[11] - clip[ 8];
	frust[0][3] = clip[15] - clip[12];

	// normalize the result
	t = sqrtf(frust[0][0] * frust[0][0] + frust[0][1] * frust[0][1] + frust[0][2] * frust[0][2]);
	frust[0][0] /= t;
	frust[0][1] /= t;
	frust[0][2] /= t;
	frust[0][3] /= t;

	// extract the numbers for the LEFT plane
	frust[1][0] = clip[ 3] + clip[ 0];
	frust[1][1] = clip[ 7] + clip[ 4];
	frust[1][2] = clip[11] + clip[ 8];
	frust[1][3] = clip[15] + clip[12];

	// normalize the result
	t = sqrtf(frust[1][0] * frust[1][0] + frust[1][1] * frust[1][1] + frust[1][2] * frust[1][2]);
	frust[1][0] /= t;
	frust[1][1] /= t;
	frust[1][2] /= t;
	frust[1][3] /= t;

	// extract the BOTTOM plane
	frust[2][0] = clip[ 3] + clip[ 1];
	frust[2][1] = clip[ 7] + clip[ 5];
	frust[2][2] = clip[11] + clip[ 9];
	frust[2][3] = clip[15] + clip[13];

	// normalize the result
	t = sqrtf(frust[2][0] * frust[2][0] + frust[2][1] * frust[2][1] + frust[2][2] * frust[2][2]);
	frust[2][0] /= t;
	frust[2][1] /= t;
	frust[2][2] /= t;
	frust[2][3] /= t;

	// extract the TOP plane
	frust[3][0] = clip[ 3] - clip[ 1];
	frust[3][1] = clip[ 7] - clip[ 5];
	frust[3][2] = clip[11] - clip[ 9];
	frust[3][3] = clip[15] - clip[13];

	// normalize the result
	t = sqrtf(frust[3][0] * frust[3][0] + frust[3][1] * frust[3][1] + frust[3][2] * frust[3][2]);
	frust[3][0] /= t;
	frust[3][1] /= t;
	frust[3][2] /= t;
	frust[3][3] /= t;

	// extract the FAR plane
	frust[4][0] = clip[ 3] - clip[ 2];
	frust[4][1] = clip[ 7] - clip[ 6];
	frust[4][2] = clip[11] - clip[10];
	frust[4][3] = clip[15] - clip[14];

	// normalize the result
	t = sqrtf(frust[4][0] * frust[4][0] + frust[4][1] * frust[4][1] + frust[4][2] * frust[4][2]);
	frust[4][0] /= t;
	frust[4][1] /= t;
	frust[4][2] /= t;
	frust[4][3] /= t;

	// extract the NEAR plane
	frust[5][0] = clip[ 3] + clip[ 2];
	frust[5][1] = clip[ 7] + clip[ 6];
	frust[5][2] = clip[11] + clip[10];
	frust[5][3] = clip[15] + clip[14];

	// normalize the result
	t = sqrtf(frust[5][0] * frust[5][0] + frust[5][1] * frust[5][1] + frust[5][2] * frust[5][2]);
	frust[5][0] /= t;
	frust[5][1] /= t;
	frust[5][2] /= t;
	frust[5][3] /= t;
}

static inline bool pt_frustum(const frustum frust, float x, float y, float z) {
	for(int p = 0; p < 6; ++p) {
		if(frust[p][0]*x + frust[p][1]*y + frust[p][2]*z + frust[p][3] <= 0.f) return 0;
	}
	return 1;
}

static inline bool sphere_frustum(const frustum frust, float x, float y, float z, float radius) {
	for(int p = 0; p < 6; ++p) {
		if(frust[p][0]*x + frust[p][1]*y + frust[p][2]*z + frust[p][3] <= -radius) return 0;
	}
	return 1;
}

// returns the distance from the sphere to the near plane
static inline float sphere_frustum_near(const frustum frust, float x, float y, float z, float radius) {
	float d;
	for(int p = 0; p < 6; ++p) {
		d = frust[p][0]*x + frust[p][1]*y + frust[p][2]*z + frust[p][3];
		if(d <= -radius) return 0;
	}
	return d + radius;
}

// size being the length/2
/*
NOTE: The function given above will produce the occasional false positive, meaning it will tell you a cube is visible when it's not. This happens in the case where all the corners of the bounding box are not behind any one plane, but it's still outside the frustum. So you might end up rendering objects that won't be visible. Depending on how your world looks and how you're using frustum culling, this is likely to be a rare event, and it shouldn't have a noticeable impact on overall rendering speed unless the false positives contain a very large number of polygons.

To make this completely accurate you would also have to test the eight corners of the frustum volume against the six planes that make up the sides of the bounding box. If the bounding box is axially aligned, you can dispense with the box's planes and perform some simple greater-than/less-than tests for each corner of the frustum. In any case, this is left as an exercise for those with more patience than I :-)
*/
static inline bool cube_frustum(const frustum frust, float x, float y, float z, float size) {
	for(int p = 0; p < 6; ++p) {
		if(frust[p][0]*(x - size) + frust[p][1]*(y - size) + frust[p][2]*(z - size) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x + size) + frust[p][1]*(y - size) + frust[p][2]*(z - size) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x - size) + frust[p][1]*(y + size) + frust[p][2]*(z - size) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x + size) + frust[p][1]*(y + size) + frust[p][2]*(z - size) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x - size) + frust[p][1]*(y - size) + frust[p][2]*(z + size) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x + size) + frust[p][1]*(y - size) + frust[p][2]*(z + size) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x - size) + frust[p][1]*(y + size) + frust[p][2]*(z + size) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x + size) + frust[p][1]*(y + size) + frust[p][2]*(z + size) + frust[p][3] > 0) continue;
		return 0;
	}
	return 1;
}

// size_* being the side's length/2
/*
NOTE: The function given above will produce the occasional false positive...
*/
static inline bool rect_frustum(const frustum frust, float x, float y, float z, float size_x, float size_y, float size_z) {
	for(int p = 0; p < 6; ++p) {
		if(frust[p][0]*(x - size_x) + frust[p][1]*(y - size_y) + frust[p][2]*(z - size_z) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x + size_x) + frust[p][1]*(y - size_y) + frust[p][2]*(z - size_z) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x - size_x) + frust[p][1]*(y + size_y) + frust[p][2]*(z - size_z) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x + size_x) + frust[p][1]*(y + size_y) + frust[p][2]*(z - size_z) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x - size_x) + frust[p][1]*(y - size_y) + frust[p][2]*(z + size_z) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x + size_x) + frust[p][1]*(y - size_y) + frust[p][2]*(z + size_z) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x - size_x) + frust[p][1]*(y + size_y) + frust[p][2]*(z + size_z) + frust[p][3] > 0) continue;
		if(frust[p][0]*(x + size_x) + frust[p][1]*(y + size_y) + frust[p][2]*(z + size_z) + frust[p][3] > 0) continue;
		return 0;
	}
	return 1;
}


// taken from https://www.opengl.org/wiki/GluProject_and_gluUnProject_code
static inline int project(float objx, float objy, float objz, f44 cam_view, f44 cam_proj, int viewport[4], f3 out_wc) {
	float* modelview = &cam_view[0][0];
	float* projection = &cam_proj[0][0];
	//Transformation vectors
	float fTempo[8];
	//Modelview transform
	fTempo[0] = modelview[0]*objx + modelview[4]*objy + modelview[8]*objz + modelview[12];  //w is always 1
	fTempo[1] = modelview[1]*objx + modelview[5]*objy + modelview[9]*objz + modelview[13];
	fTempo[2] = modelview[2]*objx + modelview[6]*objy + modelview[10]*objz + modelview[14];
	fTempo[3] = modelview[3]*objx + modelview[7]*objy + modelview[11]*objz + modelview[15];
	//Projection transform, the final row of projection matrix is always [0 0 -1 0]
	//so we optimize for that.
	fTempo[4] = projection[0]*fTempo[0] + projection[4]*fTempo[1] + projection[8]*fTempo[2] + projection[12]*fTempo[3];
	fTempo[5] = projection[1]*fTempo[0] + projection[5]*fTempo[1] + projection[9]*fTempo[2] + projection[13]*fTempo[3];
	fTempo[6] = projection[2]*fTempo[0] + projection[6]*fTempo[1] + projection[10]*fTempo[2] + projection[14]*fTempo[3];
	fTempo[7] = -fTempo[2];
	//The result normalizes between -1 and 1
	if(f_eq(fTempo[7], 0.0f)) return 0; //The w value
	fTempo[7] = 1.0/fTempo[7];
	//Perspective division
	fTempo[4] *= fTempo[7];
	fTempo[5] *= fTempo[7];
	fTempo[6] *= fTempo[7];
	//Window coordinates
	//Map x, y to range 0-1
	out_wc[0] = (fTempo[4]*0.5 + 0.5)*viewport[2] + viewport[0];
	out_wc[1] = (fTempo[5]*0.5 + 0.5)*viewport[3] + viewport[1];
	//This is only correct when glDepthRange(0.0, 1.0)
	out_wc[2] = (1.0 + fTempo[6])*0.5;	//Between 0 and 1
	return 1;
}

static inline int unproject(float winx, float winy, float winz, f44 inv_cam_projview, int viewport[4], f3 out_ws) {
	vec4 result;
	result[0] = (winx - (float)viewport[0])/(float)viewport[2]*2.0f - 1.0f;
	result[1] = (winy - (float)viewport[1])/(float)viewport[3]*2.0f - 1.0f;
	result[2] = 2.0f*winz - 1.0f;
	result[3] = 1.0f;

	m4_mul_v4(result, inv_cam_projview, result);
	if(f_eq(result[3], 0.0f)) return 0;
	result[3] = 1.0f/result[3];

	out_ws[0] = result[0]*result[3];
	out_ws[1] = result[1]*result[3];
	out_ws[2] = result[2]*result[3];
	return 1;
}

#endif
